import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { ErrorMessage } from "@hookform/error-message";
import Countdown from "react-countdown";
import Alert from "react-bootstrap/Alert";

export default function (props) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {},
  });

  const navigate = useNavigate();

  const [checkcount, setCheckCount] = useState(false);

  const [count, setCount] = useState(0);

  const [chance, setChance] = useState(2);

  const onSubmit = (data) => {
    if (data.email === "joicejssc@gmail.com" && data.password === "password") {
      alert("berhasil");
      navigate("/home");
    } else {
      alert("You have " + chance + " chances");
      setCount(count + 1);
      setChance(chance - 1);
    }
  };

  const renderer = ({ seconds, completed }) => {
    if (completed) {
      // Render a completed state
      setCount(0);
      setCheckCount(false);
      setChance(2);
    } else {
      // Render a countdown
      setCheckCount(true);
      return <span>{seconds}</span>;
    }
  };

  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit(onSubmit)}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="form-group mt-3">
            <label>Email address</label>
            <input
              {...register("email", { required: "This is required" })}
              type="email"
              className="form-control mt-1"
              placeholder="Enter email"
            />
            <ErrorMessage
              errors={errors}
              name="email"
              render={({ message }) => (
                <Alert key={"danger"} variant={"danger"}>
                  {message}
                </Alert>
              )}
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              {...register("password", { required: "This is required" })}
              type="password"
              className="form-control mt-1"
              placeholder="Enter password"
            />
            <ErrorMessage
              errors={errors}
              name="password"
              render={({ message }) => (
                <Alert key={"danger"} variant={"danger"}>
                  {message}
                </Alert>
              )}
            />
          </div>

          <div className="d-grid gap-2 mt-3">
            <button
              type="submit"
              className="btn btn-primary"
              disabled={checkcount}
            >
              Submit
            </button>
          </div>
          {count === 3 ? (
            <>
              Try again in{" "}
              <Countdown date={Date.now() + 30000} renderer={renderer} />
            </>
          ) : (
            <></>
          )}
        </div>
      </form>
    </div>
  );
}
