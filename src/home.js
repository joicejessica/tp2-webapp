import React, { useState, useEffect } from 'react'
import { useNavigate } from "react-router-dom";
import { useIdleTimer } from 'react-idle-timer'

function Home() {

    const timeout = 30000
    const [remaining, setRemaining] = useState(timeout)
    const [isIdle, setIsIdle] = useState(false)

    const navigate = useNavigate();
  
    const handleOnActive = () => setIsIdle(false)
    const handleOnIdle = () => setIsIdle(true)
  
    const {
      getRemainingTime,
    } = useIdleTimer({
      timeout,
      onActive: handleOnActive,
      onIdle: handleOnIdle
    })

  
    useEffect(() => {
      setRemaining(getRemainingTime())
  
      setInterval(() => {
        setRemaining(getRemainingTime())
      }, 1000)
    }, [])

    if(isIdle === true){
        navigate("/")
    }

  return (
    <div>
      <div>
        <h1>Timeout: {timeout}ms</h1>
        <h1>Time Remaining: {remaining}</h1>
        <h1>Idle: {isIdle.toString()}</h1>
      </div>
    <div>Login success</div>
    </div>
  )
}

export default Home